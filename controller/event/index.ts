
import {Request, Response} from 'express'
import Event from '../../models/Event';

const Events = {

    create : async (req:Request , res:Response ):Promise<any> => {
        try {
          
            
            const {name , startDate , endDate, location, description , dates} = req.body;
    
            // check if the event is exists
            const event  = await Event.findOne({name : name})  ;
            if (event) {
             res.status(400).json({ errors:true, msg: 'Event is Already Exists' });
            } else {
                // save event to db
                const eventCollection =   new Event({ name, startDate, endDate, location, description , dates}) ;
                await eventCollection.save();
                
              res.status(200).json({ msg: 'Event Created', eventCollection });
                };
               
              
            }
            catch (error:any) {
                res.status(500).json({errors : [{msg : error.message}]});
            }
        },
    remove : async (req:Request , res:Response ): Promise<any> => {
        try {
            
            const name :string = req.body.name;
            // check if the event is exists
            let event  = await Event.findOneAndDelete({name : name})  ;
            if (!event) {
                return res.status(400).json({ errors: [{ msg: 'There is no event' }] });
            } else {
                res.status(200).json({ msg: 'Event Deleted', event });
            }
            
        }catch (error:any) {
            res.status(500).json({errors : true,msg : error.message});
        }
    },
    getSingle : async (req :Request, res:Response ): Promise<any> => {
        try {
            
             const name :string = req.body.name;
    
            // check if the event is exists
            let event  = await Event.findOne({name : name})  ;
            if (!event) {
                 res.status(400).json({ errors: [{ msg: 'There is no event with this name' }] });
            } else {
                res.status(200).json({ msg: 'Event found', event });
            }
            
        }catch (error:any) {
            res.status(500).json({errors : [{msg : error.message}]});
        }
    },
    getAll : async (req:Request , res:Response ):Promise<any> => {
        try {
            
    
            // check if the event is exists
            const event  = await Event.find()  ;
            if (!event) {
                return res.status(400).json({ errors:true, msg: 'There is no event'  });
            } else {
                res.status(200).json({ msg: 'Events found', event });
            }
            
        }catch (error:any) {
            res.status(500).json({errors : [{msg : error.message}]});
        }
    },
    update : async (req:Request, res:Response):Promise<any> => {
    try {
      if (req.body.customField) {
        const { customField, name ,data } = req.body;
        const filter = { name: name };
        const update = { [customField]: data };
        const event = await Event.findOneAndUpdate(filter, update, {
          returnOriginal: false,
        });
        res.status(200).json({
          msg: "event Updated",
           event,
        });
      } else {
        const { data } = req.body;
        if (!data) {
          return res.status(400).json({
            msg: "The data send is incorrect",
          });
        }
        const filter = { name: data.name };
        const event = await Event.findOneAndUpdate(filter, data, {
          returnOriginal: false,
        });
        res.status(200).json({
            msg: "event Updated",
             event,
          });
      }
    } catch (error:any) {
      res.status(500).json({ errors: [{ msg: error.message }] });
    }
  }
}

export default Events