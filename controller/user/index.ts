import express,{Response,Request} from 'express';
import {loginUser} from "../../interface/user";
import User from "../../models/User"
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import sendEmail from "../../utils/email";
import jwt_decode from "jwt-decode";


const Users = {

    register : async (req:Request , res:Response):Promise<any> => {

    if (req.body.googleAccessToken) {
      try {
        const { googleAccessToken } = req.body;
        var decoded: any = jwt_decode(googleAccessToken);
        const user = await User.findOne({ email: decoded.email });
        if (!user) {
          const userr = new User({
            name: decoded.name,
            email: decoded.email,
            verified: true,
          });
          await userr.save();
          // save user to db
          const payload = {
            user: {
              id: userr._id,
              name: userr.name,
            },
          };
          if (process.env.JWT_SECRET_KEY) {
            jwt.sign(
              payload,
              process.env.JWT_SECRET_KEY,
              { expiresIn: "30d" },
              async (err, token) => {
                if (token) {
                  const link = `https://team-six-backend-git-development-team-sahil.vercel.app/api/users/verify?token=${token}`;
                  await sendEmail(
                    decoded.email,
                    `Welcome  ${userr.name}`,
                    link,
                    "emailVerification.ejs.html"
                  );
                  res.status(201).json({
                    user: userr,
                    token: token,
                  });
                } else {
                  throw err;
                }
              }
            );
          }
        } else {
          const payload = {
            user: {
              id: user._id,
              name: user.name,
            },
          };
          if (process.env.JWT_SECRET_KEY) {
            jwt.sign(
              payload,
              process.env.JWT_SECRET_KEY,
              { expiresIn: "30d" },
              async (err, token) => {
                if (token) {
                  res.status(201).json({
                    user: user,
                    token: token,
                  });
                } else {
                  throw err;
                }
              }
            );
          }
        }
      } catch (error) {
      }
    } else if (req.body.githubAccess) {
      try {
        const { githubAccess } = req.body;
        const user = await User.findOne({ email: githubAccess.email });
        if (!user) {
          const userr = new User({
            name: githubAccess.name,
            email: githubAccess.email,
            verified: true,
          });
          await userr.save();
          // save user to db
          const payload = {
            user: {
              id: userr._id,
              name: userr.name,
            },
          };
          if (process.env.JWT_SECRET_KEY) {
            jwt.sign(
              payload,
              process.env.JWT_SECRET_KEY,
              { expiresIn: "30d" },
              async (err, token) => {
                if (token && userr.email) {
                  const link = `https://team-six-backend-git-development-team-sahil.vercel.app/api/users/verify?token=${token}`;
                  await sendEmail(
                    userr.email,
                    `Welcome  ${userr.name}`,
                    link,
                    "emailVerification.ejs.html"
                  );
                  res.status(201).json({
                    user: userr,
                    token: token,
                  });
                } else {
                  throw err;
                }
              }
            );
          }
        } else {
          const payload = {
            user: {
              id: user._id,
              name: user.name,
            },
          };
          if (process.env.JWT_SECRET_KEY) {
            jwt.sign(
              payload,
              process.env.JWT_SECRET_KEY,
              { expiresIn: "30d" },
              async (err, token) => {
                if (token) {
                  res.status(201).json({
                    user: user,
                    token: token,
                  });
                } else {
                  throw err;
                }
              }
            );
          }
        }
      } catch (error) {
      }
    } else {
      try {
        const { name, email, password } = req.body;
        
        // check if the user is exists
        let user = await User.findOne({ email: email });
        if (user) {
          return res
            .status(400)
            .json({ errors: [{ msg: "User is Already Exists" }] });
        } else {
          // set the avatar for user
        
          // encode the password
          const salt = await bcrypt.genSalt(10);
          const passwordHash = await bcrypt.hash(password, salt);
        
          // save user to db
          user = new User({
            name,
            email,
            password: passwordHash,
          });
          await user.save();
          let payload = {
            user: {
              id: user._id,
              name: user.name,
            },
          };
          if (process.env.JWT_SECRET_KEY) {
            jwt.sign(
              payload,
              process.env.JWT_SECRET_KEY,
              { expiresIn: "30d" },
              async (err, token) => {
                if (token) {
                  const link = `https://team-six-backend-git-development-team-sahil.vercel.app/api/users/verify?token=${token}`;
                  await sendEmail(
                    email,
                    "Verify Email",
                    link,
                    "emailVerification.ejs.html"
                  );
                } else {
                  throw err;
                }
              }
            );
          }
          res.status(201).json({
            msg: "Registration is Successful and Verification Email Send",
          });
        }
      } catch (error: any) {
        res.status(500).json({ errors: [{ msg: error.message }] });
      }
    }

  },
  addUser: async (req: Request, res: Response): Promise<any> => {

    try {
      const { name, email, password } = req.body;
        
      // check if the user is exists
      let user = await User.findOne({ email: email });
      if (user) {
        return res
          .status(400)
          .json({ errors: [{ msg: "User is Already Exists" }] });
      } else {
        // set the avatar for user
        
        // encode the password
        const salt = await bcrypt.genSalt(10);
        const passwordHash = await bcrypt.hash(password, salt);
        
        // save user to db
        user = new User({
          name,
          email,
          password: passwordHash,
        });
        await user.save();
        let payload = {
          user: {
            id: user._id,
            name: user.name,
          },
        };
         
        res.status(201).json({
          msg: "Registration is Successful ",
        });
      }
    } catch (error: any) {
      res.status(500).json({ errors: [{ msg: error.message }] });
    }
  },

    login : async (req:Request   , res:Response  ) => {
        try {
            const {email , password} = req.body;
            const user  = await User.findOne({email : email})  ;
            if (!user) {
                 res.status(401).json({ errors: [{ msg: 'Invalid Credentials' }] })
            }
            else {
                // check password
                const isMatch = await bcrypt.compare(password, user?.password as string);
                if (!isMatch) {
                     res.status(401).json({ errors: [{ msg: 'Invalid Credentials' }] })
                }
    
                // create a token
                const payload = {
                    user: {
                        id: user.id,
                        name: user.name,
                    }
                };
                if (process.env.JWT_SECRET_KEY) {
                    jwt.sign(payload, process.env.JWT_SECRET_KEY, { expiresIn: "7d" }, (err:any, token?:string) => {
                        if (err) throw err;
                        res.status(200).json({
                            msg: 'Login is Success',
                            token: token
                        });
                    })
                }
            }
        }
        catch (error:any) {
       
            res.status(500).json({errors :true ,msg : error.message});
        }
    },
    verify : async (req:Request, res:Response) => {
        try {
            const userid:{user:{id:string,name:string,email:string}}=req.body.decode
            const user = await User.findById(userid.user.id)  ;
            if (!user) return res.status(400).send("Invalid link");
            user.verified = true;
            await user.save()
            res.status(200).json({
                msg: 'Email Verified',
                User: user
            });
      
        } catch (error) {
            res.status(400).send("An error occured");
        }
    },
    remove : async (req:Request, res:Response):Promise<any> => {
        try {
          const { email } = req.body;
            const user = await User.findOneAndDelete({email : email});
            if (!user) {res.status(400).send("There is no user with this Email");}
            res.status(200).json({
                msg: 'User Deleted',
                User: user
            });
      
        } catch (error) {
            res.status(400).send("An error occured");
        }
    },
    getSingle : async (req:Request, res:Response):Promise<any> => {
        try {
            const userId= req.body.user;            
            const user = await User.findById(userId.user.id);
            if (!user) return res.status(400).send("There is no user with this Email");
            res.status(200).json({
                msg: 'User Details',
                User: user
            });
      
        } catch (error) {
            res.status(400).send("An error occured");
        }
    },
    getAll : async (req:Request, res:Response):Promise<any> => {
        try {
            const user = await User.find();
            if (!user) return res.status(400).json({error:true,msg:"There is no user"});
            res.status(200).json({
                msg: 'Users Found',
                User: user
            });
      
        } catch (error) {
            res.status(400).send("An error occured");
        }
    },
    update : async (req:Request, res:Response) => {
        try {
            console.log(req.body.customField);
            
          if (req.body.customField != undefined) {
            const { customField, email ,data } = req.body;
            const filter = { email: email };
            if (customField == "password") {
              const salt = await bcrypt.genSalt(10);
              const passwordHash = await bcrypt.hash(data, salt);
                const updatePassword = passwordHash;
                const update = { [customField]: updatePassword };
                 const user = await User.findOneAndUpdate(filter, update, {
              returnOriginal: false,
            });
            res.status(200).json({
              msg: "User Updated",
              user: user,
            });
            }
            const update = { [customField]: data };
            const user = await User.findOneAndUpdate(filter, update, {
              returnOriginal: false,
            });
            res.status(200).json({
              msg: "User Updated",
              user: user,
            });
          } else {
            
            const { updateCollectionData } = req.body;
            if (!updateCollectionData) {
              return res.status(400).json({
                msg: "The data send is incorrect",
              });
            }
            const filter:{email:string} = { email: updateCollectionData.email };
              const user = await User.findOneAndUpdate(filter, updateCollectionData, {
              returnOriginal: false,
              });
              if (!user) {
                   res.status(400).json({
              msg: "user not updated",
              error:true
            });
              }
            res.status(200).json({
              msg: "User Updated",
              user: user,
            });
          }
        } catch (error:any) {
          res.status(500).json({ errors: true, msg: error.message });
        }
      }
}

export default Users