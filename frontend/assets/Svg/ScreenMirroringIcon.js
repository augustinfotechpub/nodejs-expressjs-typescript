import * as React from "react";

const ScreenMirroringIcon = () => (
  <svg
    width="25"
    height="25"
    stroke="black"
    strokeWidth="10"
    viewBox="0 120 700 350"
    xmlns="http://www.w3.org/2000/svg"
    xmlnsXlink="http://www.w3.org/1999/xlink"
  >
    <path d="M582.36 225.39v239.34H250.08V225.39zm-13.941 13.941h-304.4v211.45h304.4zm-104.56-146.39v117.01h-13.941v-103.07h-318.34v225.39h102.54v13.941h-116.48V92.932z" />
  </svg>
);

export default ScreenMirroringIcon;
