import * as React from "react";
import { SVGProps } from "react";

const SvgComponent = (props: SVGProps<SVGSVGElement>) => (
  <svg
    width={16}
    height={16}
    viewBox="0 0 16 16"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <title>{"subtask"}</title>
    <g transform="translate(1 1)" fill="none" fillRule="evenodd">
      <rect fill="#4BAEE8" width={14} height={14} rx={2} />
      <rect stroke="#FFF" x={3} y={3} width={5} height={5} rx={0.8} />
      <rect
        stroke="#FFF"
        fill="#FFF"
        x={6}
        y={6}
        width={5}
        height={5}
        rx={0.8}
      />
    </g>
  </svg>
);

export default SvgComponent;
