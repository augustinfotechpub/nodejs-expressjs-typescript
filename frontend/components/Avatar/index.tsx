import React from "react";
import { Avatar, Tooltip } from "antd";
import { MemberInterface, MembersInterface } from "interface";

const Members = ({ members }: MembersInterface) => {
  return (
    <>
      <Avatar.Group>
        {members?.map((item: MemberInterface, index: number) => {
          return (
            <Tooltip
              title={item?.name}
              placement="top"
              color={"#2db7f5"}
              key={index}
            >
              <Avatar src={item?.avatar} />
            </Tooltip>
          );
        })}
      </Avatar.Group>
    </>
  );
};

export default Members;
