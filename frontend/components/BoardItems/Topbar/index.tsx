import React from "react";
import { useSelector } from "react-redux";
import { useRouter } from "next/router";
import { LogoutOutlined, SearchOutlined } from "@ant-design/icons";

import { Image, Tooltip } from "antd";
import Cookies from "js-cookie";

const TopBar=() =>{
  const router = useRouter();
  const logout = () => {
    Cookies.remove("loggedin");
    localStorage.clear();
    router.push("/login");
  };

  const user = useSelector((state: any) => state?.user?.data?.user);

  return (
    <div className="h-16 ml-44 fixed bg-white w-10/12 flex items-center justify-between pr-5 rounded-2xl mt-4 z-50 shadow-md">
      <div className="flex px-5 items-center gap-4">
        <SearchOutlined className="w-5 h-5 text-[#65676d]" />
        <input
          type="text"
          placeholder="Search for Best Events...."
          className=" bg-transparent border-0 text-[#65676d] placeholder-gray-200"
        />
      </div>
      <div className="flex space-x-6">
        <div className="flex items-center text-[#65676d] gap-4">
          <h3 className="font-bold mr-3">{user?.name}</h3>
          <Tooltip placement="bottom" title={"Preview"} color={"#2db7f5"}>
            <Image width={32} height={32} src={user?.avatar} />
          </Tooltip>
          <LogoutOutlined className="cursor-pointer" onClick={logout} />
        </div>
      </div>
    </div>
  );
}

export default TopBar;
