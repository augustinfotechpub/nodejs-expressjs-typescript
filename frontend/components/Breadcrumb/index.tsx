import React from "react";
import Link from "next/link";
import { Breadcrumb } from "antd";
import { BreadcrumbTabProps } from "interface";

const BreadcrumbTab = ({ itemRender, ticketNumber }: BreadcrumbTabProps) => (
  <Breadcrumb>
    <Breadcrumb.Item>
      <Link href={"/"}> Home</Link>
    </Breadcrumb.Item>
    <Breadcrumb.Item>{itemRender}</Breadcrumb.Item>
    <Breadcrumb.Item>{ticketNumber}</Breadcrumb.Item>
  </Breadcrumb>
);

export default BreadcrumbTab;
