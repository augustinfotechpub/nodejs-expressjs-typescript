/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: false,
  swcMinify: true,
  optimizeFonts: false,
  images: {
    domains: ["randomuser.me", "avataaars.io"],
  },
};

module.exports = nextConfig;
