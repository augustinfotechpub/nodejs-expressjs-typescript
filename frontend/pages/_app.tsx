import { useEffect, useState } from "react";
import { Provider } from "react-redux";
import { QueryClient, QueryClientProvider } from "react-query";
import { useRouter } from "next/router";
import NextNProgress from "nextjs-progressbar";
import type { AppProps } from "next/app";
import { GoogleOAuthProvider } from "@react-oauth/google";
import { LoaderContext } from "context/loader";
import Loader from "components/Loader";
import { store } from "store/store";
import { useAuthentication } from "security";
import { PersistGate } from "redux-persist/integration/react";
import { persistStore } from "redux-persist";
import "styles/tailwind.css";
import "../styles/globals.css";
import "antd/dist/reset.css";

const queryClient = new QueryClient();
let persistor = persistStore(store);
export default function App({ Component, pageProps }: AppProps) {
  const router = useRouter();

  const [loader, setLoader] = useState(false);

  const { authenticated, redirect, secure } = useAuthentication();

  useEffect(() => {
    if (redirect && authenticated) {
      router.push("/dashboard");
    } else if (redirect) {
      router.push("/login");
    }
  }, [redirect]);

  return (
    <>
      {loader && <Loader />}
      <NextNProgress
        color="#036aa7"
        startPosition={0.3}
        stopDelayMs={400}
        height={3}
        showOnShallow={true}
      />
      <Provider store={store}>
        <GoogleOAuthProvider
          clientId={`93374628709-g9q31r7sglvqi0acf55ce1j8cthlkbva.apps.googleusercontent.com`}
        >
          <LoaderContext.Provider value={{ loader, setLoader }}>
            <QueryClientProvider client={queryClient}>
              <PersistGate persistor={persistor}>
                <Component {...pageProps} />
              </PersistGate>
            </QueryClientProvider>
          </LoaderContext.Provider>
        </GoogleOAuthProvider>
      </Provider>
    </>
  );
}
