export const ROUTES = {
  HOME: {
    LINK: "/",
    TITLE: "Home",
    SECURE: true,
  },
  LOGIN: {
    LINK: "/login",
    TITLE: "Login",
    SECURE: false,
  },
  REGISTER: {
    LINK: "/register",
    TITLE: "Register",
    SECURE: false,
  },
  FORGOTPASSWORD: {
    LINK: "/forgotpassword",
    TITLE: "Register",
    SECURE: false,
  },
  DASHBOARD: {
    LINK: "/dashboard",
    TITLE: "Dashboard",
    SECURE: true,
  },
  MAIN: {
    LINK: "/main",
    TITLE: "Main",
    SECURE: true,
  },
  WORKSPACE: {
    LINK: "/workspace",
    TITLE: "Workspace",
    SECURE: true,
  },
  EDITPROFILE: {
    LINK: "/editprofile",
    TITLE: "Edit profile",
    SECURE: true,
  },
  MAINBOARD: {
    LINK: "/mainboard",
    TITLE: "Edit profile",
    SECURE: true,
  },
  PROJECTDASHBOARD: {
    LINK: "/projectDashboard",
    TITLE: "Project Dashboard ",
    SECURE: true,
  },
  BACKLOG: {
    LINK: "/backlog",
    TITLE: "Backlog",
    SECURE: true,
  },
};
