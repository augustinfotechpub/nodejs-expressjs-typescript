import styled from "styled-components";

export const MainContainer = styled.div``;

export const Header = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  background: #036aa7;
  padding: 10px;

  .anticon-appstore svg {
    filter: invert(1);
  }
`;
export const LogoName = styled.div`
  color: #fff;
  font-weight: 700;
  font-size: 1.25rem;
  padding-left: 20px;
`;

export const Logo = styled.div`
  display: flex;
  align-items: center;
`;

export const NavBar = styled.div`
  display: flex;
  justify-content: space-around;
  width: 50%;
  align-items: center;
`;

export const NavList = styled.li`
  list-style: none;
  color: #fff;
  font-size: 1rem;
  font-weight: 500;
`;

export const CreateButton = styled.button`
  color: #fff;
  background-color: #064a74;
  padding: 10px 20px;

  :hover {
    background-color: #19557b;
  }
`;
export const CustomStyle = styled.div`
  display: flex;
`;
export const ModalLeftSide = styled.div`
  width: 50%;
  padding: 15px;
`;

export const CloseModalButton = styled.div`
  display: flex;
  justify-content: end;
  cursor: pointer;
  padding: 15px;
`;

export const ModalRightSide = styled.div`
  width: 50%;
  background-color: #e5f7fa;
`;

export const ModalImage = styled.img``;

export const ModalRightContent = styled.div`
  display: flex;
  justify-content: center;
  padding: 40px;

  .img {
    object-fit: contain;
    width: 350px;
  }
`;

export const ModalHeading = styled.div`
  font-size: 1.4rem;
  font-weight: 700;
  color: #081f42;
  padding-bottom: 10px;
`;

export const ModalSubHeading = styled.div`
  font-size: 1rem;
  color: #6b778c;
  padding-bottom: 15px;
`;
export const InputHeading = styled.div`
  font-size: 0.8rem;
  font-weight: 700;
  color: #081f42;
  padding: 10px 0px;
`;

export const InputSubHeading = styled.div`
  font-size: 0.8rem;
  color: #6b778c;
  padding: 10px 0;
`;

export const ModalButton = styled.div`
  padding: 10px 20px;
  background-color: #f6f6f7;
  color: #aeb5c1;
  font-size: 1rem;
  display: flex;
  justify-content: center;
  border-radius: 5px;
  cursor: pointer;

  :hover {
    background-color: #eee;
  }
`;

export const RightHeader = styled.div`
  display: flex;
  align-items: center;
  gap: 1rem;
  padding-right: 20px;
  p {
    color: white;
    font-weight: bold;
  }
  input {
    border: 1px solid #0e5682;
  }
  span button {
    border: 2px solid #0e5682;
  }

  .anticon-bell svg,
  .anticon-question-circle svg {
    filter: invert(1);
  }
`;

export const BodyContainer = styled.div`
  display: flex;
  margin: 0 auto;
  padding: 25px 0;
  gap: 50px;
`;

export const LeftSidebar = styled.div`
  max-width: 300px;
  width: 100%;
  padding: 30px;
`;

export const RightContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

export const SidebarList = styled.div`
  border-bottom: 2px solid #e2e3e4;
`;

export const LeftList = styled.div`
  padding: 15px;
  color: #064a74;
  font-weight: 700;
  display: flex;

  :hover {
    background-color: #e4f0f6;
  }

  .anticon-insert-row-right,
  .anticon-container,
  .anticon-home {
    padding-right: 10px;
  }
`;

export const WorkspaceContainer = styled.div`
  display: flex;
  flex-direction: column;
  gap: 10px;
  font-weight: 700;
  color: #5f6d85;
  padding: 20px;
`;

export const WorkspaceButton = styled.button`
  :focus {
    outline: none !important;
  }
`;
export const WorkspaceHeading = styled.div`
  padding-top: 10px;
`;

export const MainContent = styled.div`
  display: flex;
  flex-direction: column;
  gap: 10px;
`;

export const MainHeading = styled.div`
  font-weight: 800;
  font-size: 20px;
  color: #273859;
  display: flex;
  align-items: center;
  gap: 10px;

  .anticon-clock-circle {
    padding-right: 10px;
  }
`;

export const RecentView = styled.div`
  color: #fff;
  display: flex;
  flex-wrap: wrap;
  gap: 25px;
`;

export const RecentImg = styled.img`
  height: 150px;
`;

export const RecentHeading = styled.div`
  font-weight: 700;
  font-size: 1.2rem;
  top: 10px;
  text-shadow: 4px 4px 2px rgba(150, 150, 150, 1);
  position: absolute;
  padding-left: 20px;
  color: #ffffff;
`;
export const WorkspaceMain = styled.div`
  color: #5e6c84;
`;

export const WorkspaceContent = styled.div`
  padding-bottom: 30px;
  font-size: 1rem;
`;
export const WorkspaceMainHeading = styled.div`
  font-weight: 700;
  padding: 30px 0;
  text-transform: uppercase;
`;
export const GuestHeading = styled.div`
  color: #1c2f51;
  font-weight: 700;
  padding: 30px 0;
  text-transform: uppercase;
`;

export const LadHeading = styled.div`
  display: flex;
  padding: 10px 0;
  font-weight: 700;

  .anticon-usergroup-add {
    padding-right: 10px;
  }
`;
export const LadImg = styled.img`
  height: 150px;
  margin-right: 20px;
`;
export const LadSubHeading = styled.div`
  color: #fff;
  position: absolute;
  top: 45px;
  left: 10px;
  font-weight: 700;
  font-size: 0.8rem;
`;

export const GuestSection = styled.div`
  color: rgb(13 19 47);
  padding-bottom: 20px;
  font-weight: 700;
  font-size: 1rem;
`;

export const Ladbrook = styled.div`
  position: relative;
`;

export const RecentViewSection = styled.div`
  position: relative;
  cursor: pointer;
  box-shadow: 8px 6px 5px -3px rgba(0, 0, 0, 0.75);
`;
export const IconWrapper = styled.div`
  text-align: center;
`;
export const HeadingWrapper = styled.div`
  display: flex;
  gap: 10px;
`;
