import styled from "styled-components";

export const Span = styled.span`
  color: grey;
  text-align: start;
  font-size: 1.8rem;
`;

export const Title = styled.p`
  text-align: start;
  color: black;
  font-weight: 500;
  font-size: 3rem;
`;

export const Name = styled.p`
  font-weight: 500;
  text-align: start;
  padding-bottom: 1vh;
  margin-left: 1.6rem;
  margin-top: 2vh;
`;

export const Description = styled.p`
  color: grey;
  text-align: start;
  font-size: 0.7rem;
  margin-left: 1.6rem;
  margin-top: 1vh;
  width: 22rem;
`;

export const Select = styled.select`
  width: 20rem;
  margin-left: 1.6rem;
  padding: 8px 12px;
  border-radius: 15px;
  border: 2px solid lightgray;
`;

export const Option = styled.option``;

export const Optional = styled.span`
  color: gray;
  font-weight: 200;
`;

export const ContinueButton = styled.button`
  width: 20rem;
  margin-left: 1.6rem;
  padding: 10px 10px;
  margin-top: 1vh;
  margin-bottom: 6vh;
`;
