import styled from "styled-components";

export const NavContainer = styled.div`
  display: flex;
  width: 100%;
  background: linear-gradient(to right, #333399, #ff00cc);
`;

export const NavLogo = styled.div`
  display: flex;
  width: 6rem;
  align-items: center;
  padding-left: 20px;
  color: white;

  a {
    font-size: 22px;
  }
`;

export const Wrapper = styled.div`
  display: flex;
  gap: 3vw;
  align-items: center;
  width: 70vw;
  padding: 10px;
  color: white;

  div {
    display: flex;
    align-items: center;
  }

  span {
    cursor: pointer;
  }
`;

export const Icon = styled.span`
  margin-top: 0.6vh;
  margin-left: 0.1vw;
  cursor: pointer;
`;

export const InputWrap = styled.div`
  display: flex;
  width: 22rem;
  padding: 8px 5px;

  input {
    width: 100%;
    border-radius: 5px;
    border: none;
    outline: none;
  }
`;

export const Wrap = styled.div`
  display: flex;
  align-items: center;
  gap: 10px;
  padding: 5px;
  width: 7rem;
`;

export const NotificationWrap = styled.div`
  font-size: 1.2rem;
  cursor: pointer;
  padding-left: 8px;
  color: white;
`;

export const HelpIcon = styled.div`
  font-size: 1.2rem;
  cursor: pointer;
  color: white;
`;

export const Image = styled.img`
  border-radius: 50%;
  width: 2.3rem;
  cursor: pointer;
`;
