import * as Yup from "yup";

const VALIDATION_SCHEMA = Yup.object().shape({
  email: Yup.string()
    .required("Email is mandatory")
    .email("Please enter a valid email."),
});

export { VALIDATION_SCHEMA };
