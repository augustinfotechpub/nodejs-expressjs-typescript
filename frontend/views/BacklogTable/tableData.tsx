import { ReactElement } from "react";
import BugIcon from "assets/Svg/BugIcon";
import EpicIcon from "assets/Svg/StoryIcon";
import SubTaskIcon from "assets/Svg/SubTaskIcon";
import TaskIcon from "assets/Svg/TaskIcon";
import Members from "components/Avatar";

export interface ITableData {
  id: number;
  icon: ReactElement | null;
  ticketId: string;
  priority: string;
  src: ReactElement | null;
}
export const tableData = [
  {
    id: 1,
    icon: <BugIcon />,
    name: "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
    ticketId: "TN-121",
    priority: "High",
    src: <Members />,
  },
  {
    id: 2,
    icon: <TaskIcon />,
    name: "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
    ticketId: "TN-122",
    priority: "Medium",
    src: <Members />,
  },
  {
    id: 3,
    icon: <SubTaskIcon />,
    name: "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
    ticketId: "TN-123",
    priority: "Low",
    src: <Members />,
  },
  {
    id: 4,
    icon: <EpicIcon />,
    name: "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
    ticketId: "TN-124",
    priority: "Medium",
    src: <Members />,
  },
  {
    id: 5,
    icon: <BugIcon />,
    name: "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
    ticketId: "TN-125",
    priority: "High",
    src: <Members />,
  },
  {
    id: 6,
    icon: <SubTaskIcon />,
    name: "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
    ticketId: "TN-126",
    priority: "Medium",
    src: <Members />,
  },
  {
    id: 7,
    icon: <TaskIcon />,
    name: "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
    ticketId: "TN-127",
    priority: "Low",
    src: <Members />,
  },
  {
    id: 8,
    icon: <SubTaskIcon />,
    name: "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
    ticketId: "TN-128",
    priority: "High",
    src: <Members />,
  },
  {
    id: 9,
    icon: <BugIcon />,
    name: "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
    ticketId: "TN-129",
    priority: "Low",
    src: <Members />,
  },
  {
    id: 10,
    icon: <TaskIcon />,
    name: "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
    ticketId: "TN-130",
    priority: "Medium",
    src: <Members />,
  },
  {
    id: 11,
    icon: <SubTaskIcon />,
    name: "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
    ticketId: "TN-131",
    priority: "High",
    src: <Members />,
  },
  {
    id: 12,
    icon: <BugIcon />,
    name: "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
    ticketId: "TN-132",
    priority: "Medium",
    src: <Members />,
  },
  {
    id: 13,
    icon: <TaskIcon />,
    name: "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
    ticketId: "TN-133",
    priority: "Low",
    src: <Members />,
  },
  {
    id: 14,
    icon: <SubTaskIcon />,
    name: "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
    ticketId: "TN-134",
    priority: "Medium",
    src: <Members />,
  },
];
