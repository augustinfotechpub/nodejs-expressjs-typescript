import jwt, { JwtPayload } from 'jsonwebtoken'
import { Request, Response, NextFunction } from 'express'
const verifyToken = (req: Request, res: Response, next: NextFunction): any => {
  const token = req.header('Auth-token')
  if (!token) {
    res.status(401).json({ error: true, message: 'A token is required for authentication' })
  } else {
    try {
      if (process.env.JWT_SECRET_KEY) {
      
        let decoded = jwt.verify(token, process.env.JWT_SECRET_KEY);
        req.body.user = decoded;
        
        next();
    }
    } catch (error: any) {
      res.status(500).send({ error: true, message: error.message })
    }
  }
}

export default verifyToken
