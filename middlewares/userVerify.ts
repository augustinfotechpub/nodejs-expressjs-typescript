import jwt, { JwtPayload } from "jsonwebtoken";
import { Request, Response } from "express";

let userVerify = (req :Request, res : Response, next: () => void) => {
  // get token from header
  const token = req.query;
  if (!token) {
    return res
      .status(401)
      .json({ msg: "No Token , authorization denied" });
  }

  // verify the token
  try {
    if (process.env.JWT_SECRET_KEY && token.token) {
      const Token = token.token.toString()
      const decoded = jwt.verify(Token, process.env.JWT_SECRET_KEY) as JwtPayload;
       req.body.decode  = decoded;
      next();
    }
  } catch (error) {
    res.status(401).json({ msg: "Token is not valid" });
  }
};

export default userVerify;
