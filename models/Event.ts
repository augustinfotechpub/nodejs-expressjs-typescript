import mongoose from "mongoose";

 const EventSchema = new mongoose.Schema(
  {
    name: { type: String, required: true, unique : true },
    startDate: { type: String, required: true },
    endDate: { type: String, required: true },
    location: { type: String  },
    description: { type: String },
    dates : [{
      type : String
    }]
  },
  { timestamps: true }
 );

 export interface EventSchema extends mongoose.Document 
 {
   name?: string,
   startDate?: string,
   endDate?: string,
   location?: string,
   description?: string,
   dates?: [string]
 }
;

const Event = mongoose.model<EventSchema>("event", EventSchema);
export default Event
