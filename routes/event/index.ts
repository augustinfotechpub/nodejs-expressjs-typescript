import Event from "../../controller/event";
import express from 'express'
import Authenticate from "../../middlewares/authenticate"
const router = express.Router()
// event routes

const eventCreate = router.post('/create', Event.create)
const eventRemove = router.post('/remove', Authenticate, Event.remove)
const eventGetSingle = router.post('/getSingle',Authenticate, Event.getSingle)
const eventGetAll = router.get('/getAll',Authenticate, Event.getAll)
const eventUpdate = router.post('/update',Authenticate, Event.update)

export default{ eventCreate,
    eventGetAll,
    eventGetSingle,
    eventRemove,
    eventUpdate,}