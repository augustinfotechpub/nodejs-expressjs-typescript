import express from "express"
import User from "../../controller/user";
import Authenticate from "../../middlewares/authenticate"

const router = express.Router()

// user routes 

const userRegister = router.post('/register', User.register)
const userLogin = router.post('/login', User.login)
const userEmailVerify = router.post('/verify', User.verify)
const userRemove = router.post('/remove', User.remove)
const userGetSingle = router.get('/getSingle', Authenticate, User.getSingle)
const userGetAll = router.get('/getAll', Authenticate, User.getAll)
const userUpdate = router.post('/update', Authenticate, User.update)

const addUser = router.post('/addUser', Authenticate, User.addUser)




export default {
    userGetAll,
    userRegister,
    userGetSingle,
    userLogin,
    userRemove,
    userUpdate,
    userEmailVerify,
    addUser
   
}